//
//  AggregateCannedAnimationLoader.cpp
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-24.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#include "AggregateCannedAnimationLoader.hpp"

#include <memory>

using namespace animation::Model;
using namespace std;

namespace animation {
	namespace Implementation {
		namespace DataSources {

AggregateCannedAnimationLoader::AggregateCannedAnimationLoader(
		shared_ptr<IAnimationLoader> loader1,
		shared_ptr<IAnimationLoader> loader2,
		shared_ptr<IAnimationLoader> loader3)
	: m_loader1(loader1)
	, m_loader2(loader2)
	, m_loader3(loader3)
{
}

shared_ptr<Scene> AggregateCannedAnimationLoader::getScene(uint16_t id)
{
	switch (id)
	{
  	case 0:
		return m_loader1->getScene(0);
  	case 1:
		return m_loader2->getScene(0);
  	case 2:
		return m_loader3->getScene(0);
	default:
    	return make_shared<Scene>();
	}
}

vector<Animation> AggregateCannedAnimationLoader::getAnimations(uint16_t id)
{
	switch (id)
	{
  	case 0:
		return m_loader1->getAnimations(0);
  	case 1:
		return m_loader2->getAnimations(0);
  	case 2:
		return m_loader3->getAnimations(0);
	default:
    	return vector<Animation>{Animation{}};
	}
}

} // namespace DataSources
} // namespace Implementation
} // namespace animation
