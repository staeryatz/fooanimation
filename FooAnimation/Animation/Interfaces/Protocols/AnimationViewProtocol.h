//
//  AnimationViewProtocol.h
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-24.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#ifndef AnimationViewProtocol_h
#define AnimationViewProtocol_h

#import "AnimationTypes.hpp"

#import <Foundation/Foundation.h>

using namespace animation::Model;

@protocol AnimationViewProtocol <NSObject>

- (void)loadScene:(ScenePtr)scene;
- (void)updateFrame:(AnimationFramePtr)frame;
- (void)displayText:(NSString*)text;
@end

#endif /* AnimationViewProtocol_h */
