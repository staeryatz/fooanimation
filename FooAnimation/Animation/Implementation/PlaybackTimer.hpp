//
//  PlaybackTimer.hpp
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-15.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#ifndef PlaybackTimer_hpp
#define PlaybackTimer_hpp

#include "IPlaybackTimer.hpp"

#include <Dispatch/Dispatch.h>

namespace animation {
	namespace Implementation {

using namespace Interfaces;

enum TimerState
{
	Playing,
	Paused,
	Stopped
};

class PlaybackTimer : public IPlaybackTimer
{
public:
	PlaybackTimer(double playbackIntervalSeconds);
	~PlaybackTimer();

	void registerTimerIntervalFired(TimerIntervalFiredCallback callback) override;
	void registerTimerEnded(TimerEndedCallback callback) override;

	void start() override;
	void stop() override;
	void pause() override;
	void resume() override;

	bool isActive() override;

private:
	double m_playbackIntervalSeconds;
	dispatch_source_t m_timerSource;
	TimerIntervalFiredCallback m_intervalFiredCallback;
	TimerEndedCallback m_timerEndedCallback;
	TimerState m_timerState{Stopped};

	NSObject* m_lock;
};

} // namespace Implementation
} // namespace animation

#endif /* PlaybackTimer_hpp */
