//
//  IAnimationPlayerView.hpp
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-05.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#ifndef IAnimationPlayerView_h
#define IAnimationPlayerView_h

#include <functional>

using CommandHandler = std::function<void(void)>;
using SelectAnimationHandler = std::function<void(uint16_t)>;

namespace animation {
	namespace Interfaces {

class IAnimationPlayerView
{
public:
	virtual ~IAnimationPlayerView() = default;
	virtual void registerPlayPressedHandler(CommandHandler handler) = 0;
	virtual void registerPausePressedHandler(CommandHandler handler) = 0;
	virtual void registerStopPressedHandler(CommandHandler handler) = 0;
	virtual void registerSelectAnimationHandler(SelectAnimationHandler handler) = 0;
	virtual void animationPlaybackEnded() = 0;
};

} // namespace Interfaces
} // namespace animation

#endif /* IAnimationPlayerView_h */
