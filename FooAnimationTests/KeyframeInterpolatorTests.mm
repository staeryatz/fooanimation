//
//  KeyframeInterpolatorTests.m
//  FooAnimationTests
//
//  Created by Jeffrey Bakker on 2021-12-19.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#import "KeyframeInterpolator.hpp"

#import "AnimationTypes.hpp"
#import "IncrementalSpritesAnimationLoader.hpp"

#import <XCTest/XCTest.h>

#import <cmath>
#import <memory>

using namespace animation::Implementation;
using namespace animation::Model;
using namespace std;

namespace {
constexpr float FloatComparisonTolerance = 0.00005;
}

@interface KeyframeInterpolatorTests : XCTestCase {
	unique_ptr<KeyframeInterpolator> m_uut;
}

@end

@implementation KeyframeInterpolatorTests

- (void)setUp {
	m_uut = make_unique<KeyframeInterpolator>();
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

- (Animation)createTestAnimation {

	Animation animation;

	auto frame0 = make_shared<Keyframe>();
	SpriteState sprite0f0;
	sprite0f0.location.X = -0.5;
	sprite0f0.location.Y = -1.0;
	sprite0f0.location.Z = 0.001;
	sprite0f0.color.R = 0;
	sprite0f0.color.G = 128;
	sprite0f0.color.B = 255;
	sprite0f0.color.A = 250;
	sprite0f0.scale = 0.0;
	sprite0f0.visible = true;
	frame0->spriteStates.push_back(sprite0f0);
	frame0->timestamp = DurationInSeconds(0.0);
	frame0->backgroundColor.R = 0;
	frame0->backgroundColor.G = 128;
	frame0->backgroundColor.B = 255;
	frame0->backgroundColor.A = 250;

	auto frame1 = make_shared<Keyframe>();
	SpriteState sprite0f1;
	sprite0f1.location.X = 0.5;
	sprite0f1.location.Y = 1.0;
	sprite0f1.location.Z = 0.002;
	sprite0f1.color.R = 255;
	sprite0f1.color.G = 128;
	sprite0f1.color.B = 0;
	sprite0f1.color.A = 200;
	sprite0f1.scale = 2.55;
	sprite0f1.visible = false;
	frame1->spriteStates.push_back(sprite0f1);
	frame1->timestamp = DurationInSeconds(1.0);
	frame1->backgroundColor.R = 255;
	frame1->backgroundColor.G = 128;
	frame1->backgroundColor.B = 0;
	frame1->backgroundColor.A = 200;

	animation.keyframes.push_back(frame0);
	animation.keyframes.push_back(frame1);

	return animation;
}

- (Animation)createPerformanceTestAnimation {

	Animation animation;

	for (int frames = 0; frames < 511; frames++)
	{
		auto frame = make_shared<Keyframe>();
		frame->backgroundColor.R = frames / 2;
		frame->backgroundColor.R = frames / 2;
		frame->backgroundColor.R = frames / 2;

		SpriteState sprite;
		for (int sprites = 0; sprites < 255; sprites++)
		{
			sprite.location.X = frames + sprites * 0.01;
			sprite.location.Y = frames + sprites * -0.01;
			sprite.location.Z = frames + sprites * 0.0002;
			sprite.color.R = sprites;
			sprite.color.G = sprites;
			sprite.color.B = sprites;
			sprite.scale = sprites * 0.01;
			frame->spriteStates.push_back(sprite);
		}

		frame->timestamp = DurationInSeconds(1.0 * frames);
		animation.keyframes.push_back(frame);
	}

	return animation;
}

- (void)testLocationInterpolation {

	const DurationInSeconds interval{0.10};

	auto animation = [self createTestAnimation];
	auto frame = m_uut->interpolate(animation.keyframes[0], animation.keyframes[1], interval);

	auto location = frame->spriteStates[0].location;

	const float expectedX = -0.4;
	const float expectedY = -0.8;
	const float expectedZ = 0.0011;

	XCTAssertTrue(abs(location.X - expectedX) < FloatComparisonTolerance);
	XCTAssertTrue(abs(location.Y - expectedY) < FloatComparisonTolerance);
	XCTAssertTrue(abs(location.Z - expectedZ) < FloatComparisonTolerance);
}

- (void)testLocationInterpolationMaxDuration {

	const DurationInSeconds interval{1.0};

	auto animation = [self createTestAnimation];
	auto frame = m_uut->interpolate(animation.keyframes[0], animation.keyframes[1], interval);

	auto location = frame->spriteStates[0].location;

	const float expectedX = 0.5;
	const float expectedY = 1.0;
	const float expectedZ = 0.002;

	XCTAssertTrue(abs(location.X - expectedX) < FloatComparisonTolerance);
	XCTAssertTrue(abs(location.Y - expectedY) < FloatComparisonTolerance);
	XCTAssertTrue(abs(location.Z - expectedZ) < FloatComparisonTolerance);
}

- (void)testLocationInterpolationDurationExceeded {

	const DurationInSeconds interval{1.5};

	auto animation = [self createTestAnimation];
	auto frame = m_uut->interpolate(animation.keyframes[0], animation.keyframes[1], interval);

	auto location = frame->spriteStates[0].location;

	const float expectedX = 0.5;
	const float expectedY = 1.0;
	const float expectedZ = 0.002;

	XCTAssertTrue(abs(location.X - expectedX) < FloatComparisonTolerance);
	XCTAssertTrue(abs(location.Y - expectedY) < FloatComparisonTolerance);
	XCTAssertTrue(abs(location.Z - expectedZ) < FloatComparisonTolerance);
}

- (void)testBackgroundColorInterpolation {

	const DurationInSeconds interval{0.5};

	auto animation = [self createTestAnimation];
	auto frame = m_uut->interpolate(animation.keyframes[0], animation.keyframes[1], interval);

	auto color = frame->backgroundColor;

	const uint8_t expectedR = 127;
	const uint8_t expectedG = 128;
	const uint8_t expectedB = 127;
	const uint8_t expectedA = 225;

	XCTAssertEqual(color.R, expectedR);
	XCTAssertEqual(color.G, expectedG);
	XCTAssertEqual(color.B, expectedB);
	XCTAssertEqual(color.A, expectedA);
}

- (void)testSpriteColorInterpolation {

	const DurationInSeconds interval{0.5};

	auto animation = [self createTestAnimation];
	auto frame = m_uut->interpolate(animation.keyframes[0], animation.keyframes[1], interval);

	auto color = frame->spriteStates[0].color;

	const uint8_t expectedR = 127;
	const uint8_t expectedG = 128;
	const uint8_t expectedB = 127;
	const uint8_t expectedA = 225;

	XCTAssertEqual(color.R, expectedR);
	XCTAssertEqual(color.G, expectedG);
	XCTAssertEqual(color.B, expectedB);
	XCTAssertEqual(color.A, expectedA);
}

- (void)testSpriteSizeInterpolation {

	const DurationInSeconds interval{0.5};

	auto animation = [self createTestAnimation];
	auto frame = m_uut->interpolate(animation.keyframes[0], animation.keyframes[1], interval);

	auto scale = frame->spriteStates[0].scale;

	const float expectedScale = 1.275;

	XCTAssertTrue(abs(scale - expectedScale) < FloatComparisonTolerance);
}

- (void)testSpriteVisibleDoesNotInterpolate {

	const DurationInSeconds interval{0.5};

	auto animation = [self createTestAnimation];
	auto frame = m_uut->interpolate(animation.keyframes[0], animation.keyframes[1], interval);

	auto visible = frame->spriteStates[0].visible;

	const uint8_t expectedVisible = animation.keyframes[0]->spriteStates[0].visible;

	XCTAssertEqual(visible, expectedVisible);
}

- (void)testInterpolatePerformance {

	auto animation = [self createPerformanceTestAnimation];

    // This is an example of a performance test case.
    [self measureBlock:^{

		size_t prevFrameIndex = 0;
		size_t nextFrameIndex = 1;
		for (; nextFrameIndex <= animation.keyframes.size() - 1; nextFrameIndex++)
		{
			auto prevFrame = animation.keyframes[prevFrameIndex];
			auto nextFrame = animation.keyframes[nextFrameIndex];

			for (auto tweens = 0; tweens < 10; tweens++)
			{
				DurationInSeconds interval{tweens * 0.1};
				auto tweenFrame = m_uut->interpolate(prevFrame, nextFrame, interval);
			}

			prevFrameIndex++;
		}
    }];
}

@end
