//
//  IAnimationLoader.h
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-03.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#ifndef IAnimationLoader_h
#define IAnimationLoader_h

#include "AnimationTypes.hpp"

#include <memory>
#include <vector>

namespace animation {

//namespace Model {
//struct Animation;
//struct Scene;
//}

namespace Interfaces {

class IAnimationLoader
{
public:
	virtual ~IAnimationLoader() = default;
	virtual std::shared_ptr<Model::Scene> getScene(uint16_t id) = 0;
	virtual std::vector<Model::Animation> getAnimations(uint16_t id) = 0;
};

} // namespace Interfaces
} // namespace animation

#endif /* IAnimationLoader_h */
