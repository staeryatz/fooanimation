//
//  AnimationIocModule.h
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-06.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#ifndef AnimationIocModule_h
#define AnimationIocModule_h

#include "ServiceLocator.hpp"

#include "AggregateCannedAnimationLoader.hpp"
#include "AnimationController.hpp"
#include "AnimationPlayer.hpp"
#include "AnimationPlayerViewWrapper.hpp"
#include "AnimationValidator.hpp"
#include "AnimationViewWrapper.hpp"
#include "KeyframeInterpolator.hpp"
#include "IncrementalSpritesAnimationLoader.hpp"
#include "PlaybackTimer.hpp"
#include "RandomRelationalSpritesAnimationLoader.hpp"
#include "RandomSwarmSpritesAnimationLoader.hpp"

constexpr double PlaybackTimerIntervalSeconds = 0.02;

using namespace animation::Implementation;
using namespace animation::Implementation::DataSources;
using namespace animation::Interfaces;
using namespace animation::Views;

namespace animation {
	namespace InversionOfControl {

class AnimationIocModule : public ServiceLocator::Module {
public:
  void load() override {

    bind<IKeyframeInterpolator>().to<KeyframeInterpolator>([] (SLContext_sptr slc)
    {
		return new KeyframeInterpolator();
    });

    bind<IPlaybackTimer>().to<PlaybackTimer>([] (SLContext_sptr slc)
    {
		return new PlaybackTimer(PlaybackTimerIntervalSeconds);
    });

	bind<IAnimationValidator>().to<AnimationValidator>([] (SLContext_sptr slc)
    {
		return new AnimationValidator(slc->resolve<ILogger>());
    });

    bind<IAnimationPlayer>().to<AnimationPlayer>([] (SLContext_sptr slc)
    {
		return new AnimationPlayer(
			slc->resolve<ILogger>(),
			slc->resolve<IPlaybackTimer>(),
			slc->resolve<IKeyframeInterpolator>());
    });

    bind<IAnimationController>().to<AnimationController>([] (SLContext_sptr slc)
    {
		return new AnimationController(
			slc->resolve<IAnimationValidator>(),
			slc->resolve<IAnimationLoader>("Aggregate"),
			slc->resolve<IAnimationPlayer>(),
			slc->resolve<IAnimationPlayerView>(),
			slc->resolve<IAnimationView>());
    });

    bind<IAnimationLoader>("Aggregate").to<AggregateCannedAnimationLoader>([] (SLContext_sptr slc)
    {
		return new AggregateCannedAnimationLoader(
			slc->resolve<IAnimationLoader>("Incremental"),
			slc->resolve<IAnimationLoader>("RandomRelational"),
			slc->resolve<IAnimationLoader>("RandomSwarm"));
    });

    bind<IAnimationLoader>("Incremental").to<IncrementalSpritesAnimationLoader>([] (SLContext_sptr slc)
    {
		return new IncrementalSpritesAnimationLoader();
    });

    bind<IAnimationLoader>("RandomRelational").to<RandomRelationalSpritesAnimationLoader>([] (SLContext_sptr slc)
    {
		return new RandomRelationalSpritesAnimationLoader();
    });

    bind<IAnimationLoader>("RandomSwarm").to<RandomSwarmSpritesAnimationLoader>([] (SLContext_sptr slc)
    {
		return new RandomSwarmSpritesAnimationLoader();
    });
  }
};

} // namespace InversionOfControl
} // namespace animation

#endif /* AnimationIocModule_h */
