//
//  IKeyframeInterpolator.h
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-06.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#ifndef IKeyframeInterpolator_h
#define IKeyframeInterpolator_h

#include "AnimationTypes.hpp"

using namespace animation::Model;

namespace animation {
	namespace Interfaces {

class IKeyframeInterpolator
{
public:
	virtual AnimationFramePtr interpolate(
		const KeyframePtr last, const KeyframePtr next,
		const DurationInSeconds currTime) = 0;
};

} // namespace Interfaces
} // namespace animation

#endif /* IKeyframeInterpolator_h */
