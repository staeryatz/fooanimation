//
//  LoggingIocModule.h
//  Logging
//
//  Created by Jeffrey Bakker on 2021-12-27.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#ifndef LoggingIocModule_h
#define LoggingIocModule_h

#include "ServiceLocator.hpp"

#include "ILogger.hpp"
#include "Logger.hpp"

using namespace logging::Implementation;
using namespace logging::Interfaces;

namespace logging {
	namespace InversionOfControl {

class LoggingIocModule : public ServiceLocator::Module {
public:
  void load() override {

    bind<ILogger>().to<Logger>([] (SLContext_sptr slc)
    {
		return new Logger();
    });
  }
};

} // namespace InversionOfControl
} // namespace logging

#endif /* LoggingIocModule_h */
