//
//  PlaybackTimer.cpp
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-15.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#include "PlaybackTimer.hpp"

#include "AnimationTypes.hpp"

using timer_point = std::chrono::time_point<std::chrono::steady_clock>;

namespace animation {
	namespace Implementation {

PlaybackTimer::PlaybackTimer(double playbackIntervalSeconds)
{
	m_playbackIntervalSeconds = playbackIntervalSeconds;
	m_lock = [[NSObject alloc] init];
}

PlaybackTimer::~PlaybackTimer()
{
	stop();
}

void PlaybackTimer::registerTimerIntervalFired(TimerIntervalFiredCallback callback)
{
	m_intervalFiredCallback = callback;
}

void PlaybackTimer::registerTimerEnded(TimerEndedCallback callback)
{
	m_timerEndedCallback = callback;
}

void PlaybackTimer::start()
{
	m_timerSource = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,
		dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0));

	dispatch_time_t startTime = dispatch_time(DISPATCH_TIME_NOW, 0);
	uint64_t intervalTime = (int64_t)(m_playbackIntervalSeconds * NSEC_PER_SEC);
	dispatch_source_set_timer(m_timerSource, startTime, intervalTime, 0);

	__block auto blockCallback = m_intervalFiredCallback;
	__block DurationInSeconds elapsedTotal{0.0f};
	__block timer_point lastTime = std::chrono::steady_clock::now();

	dispatch_source_set_event_handler(m_timerSource, ^{
		auto firedTime = std::chrono::steady_clock::now();

		bool shouldFireCallback = false;
		@synchronized(m_lock)
		{
			shouldFireCallback = (m_timerState == Playing);
		}

		DurationInSeconds delta = firedTime - lastTime;
		lastTime = firedTime;

		if (shouldFireCallback)
		{
			elapsedTotal += delta;
			__block TimerPlaybackInfo timerInfo;
			timerInfo.elapsed = elapsedTotal;
			__block auto mainQueueCallback = blockCallback;
			dispatch_async(dispatch_get_main_queue(), ^{
				mainQueueCallback(timerInfo);
			});
		}
	});

	dispatch_resume(m_timerSource);
	m_timerState = Playing;
}

void PlaybackTimer::stop()
{
	if (m_timerSource)
	{
		dispatch_suspend(m_timerSource);
		dispatch_cancel(m_timerSource);
		dispatch_resume(m_timerSource); // https://developer.apple.com/forums/thread/27334

		m_timerSource = nil;

		if (m_timerEndedCallback)
		{
			m_timerEndedCallback();
		}
	}

	@synchronized(m_lock)
	{
		m_timerState = Stopped;
	}
}

void PlaybackTimer::pause()
{
	@synchronized(m_lock)
	{
		m_timerState = Paused;
	}
}

void PlaybackTimer::resume()
{
	@synchronized(m_lock)
	{
		m_timerState = Playing;
	}
}

bool PlaybackTimer::isActive()
{
	return m_timerSource != nil;
}

} // namespace Implementation
} // namespace animation
