//
//  OpenGlAnimationView.m
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-03.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#import "OpenGlAnimationView.h"

#import "AnimationTypes.hpp"

using namespace animation::Model;

@implementation OpenGlAnimationView

- (void)registerPlayPressedHandler:(CommandHandlerBlock)handler {
	_playCommandHandler = handler;
}

- (void)registerPausePressedHandler:(CommandHandlerBlock)handler {
	_pauseCommandHandler = handler;
}

- (void)registerStopPressedHandler:(CommandHandlerBlock)handler {
	_stopCommandHandler = handler;
}

- (void)registerSelectAnimationHandler:(SelectAnimationHandlerBlock)handler {
	_selectAnimationHandler = handler;
}

- (void)animationPlaybackEnded {
	m_isPlaying = false;
}

-(void)keyUp:(NSEvent*)event {
//	NSLog(@"Key down: %d", event.keyCode);
}
-(void)keyDown:(NSEvent*)event {
//	NSLog(@"Key up: %d", event.keyCode);

	const UInt16 KeyCodeChar1 = 18;
	const UInt16 KeyCodeChar2 = 19;
	const UInt16 KeyCodeChar3 = 20;
	const UInt16 KeyCodeCharP = 35;
	const UInt16 KeyCodeCharS = 1;
	const UInt16 KeyCodeCharSpace = 49;

	switch (event.keyCode)
	{
	case KeyCodeChar1:
	case KeyCodeChar2:
	case KeyCodeChar3:
		_selectAnimationHandler(event.keyCode - 18);
		break;
	case KeyCodeCharP:
	case KeyCodeCharSpace:
		if (m_isPlaying)
		{
			_pauseCommandHandler();
		}
		else
		{
			_playCommandHandler();
		}
		m_isPlaying = !m_isPlaying;
		break;
	case KeyCodeCharS:
		_stopCommandHandler();
		break;
	}
}

- (BOOL)acceptsFirstResponder {
    return YES;
}

- (void)loadScene:(ScenePtr)scene {
	_scene = scene;
}

- (void)updateFrame:(AnimationFramePtr)frame {
	_currentFrame = frame;
	[self setNeedsDisplayInRect:self.bounds];
}

- (void)displayText:(NSString*)text {
	_text = text;
}

- (NSColor*)convertColor:(Color&)color {

	auto red = (CGFloat)color.R / 255;
	auto green = (CGFloat)color.G / 255;
	auto blue = (CGFloat)color.B / 255;
	auto alpha = (CGFloat)color.A / 255;

//	NSLog(@"NSColor - red:%f green:%f blue:%f", red, green, blue);
	return [NSColor colorWithRed:red green:green blue:blue alpha:alpha];
}

- (void)drawRect:(NSRect)dirtyRect {

	if (!_currentFrame)
	{
		glClearColor(0.0, 0.0, 0.0, 1.0);
		glClear(GL_COLOR_BUFFER_BIT);
		glFlush();
		return;
	}

	auto color = [self convertColor:_currentFrame->backgroundColor];
	glClearColor([color redComponent], [color greenComponent], [color blueComponent], 1.0);
    glClear(GL_COLOR_BUFFER_BIT);

//	NSLog(@"Position - X:%f Y:%f Z:%f", posX, posY, posZ);

	for (auto& sprite : _currentFrame->spriteStates)
	{
		[self drawSprite:sprite];
	}

	glFlush();
}

- (void)drawSprite:(SpriteState)spriteState {

	if (!_scene)
	{
		NSLog(@"Scene is not initialized.");
		return;
	}

	if (_scene->sprites.count(spriteState.spriteId) < 1)
	{
		NSLog(@"Invalid spriteId: %ld", spriteState.spriteId);
		return;
	}

	auto spriteColor = [self convertColor:spriteState.color];
	auto spriteModel = _scene->sprites[spriteState.spriteId];
	auto size = spriteModel.size * spriteState.scale;
	auto x = spriteState.location.X;
	auto y = spriteState.location.Y;
	auto z = spriteState.location.Z;

    glColor3f([spriteColor redComponent], [spriteColor greenComponent], [spriteColor blueComponent]);
    glBegin(GL_TRIANGLES);
    {
    	const float magicCoordinateSystemScale = 0.003; // look good for a demo
    	auto pointSize = size * magicCoordinateSystemScale;

        glVertex3f(  0.0 + x, pointSize + y, z); // top x, y, z: +x=>right, +y=>up
        glVertex3f( -pointSize + x, -pointSize + y, z); // btm-left -x=>left
        glVertex3f(  pointSize + x, -pointSize + y, z); // btm-right +x=>right
    }
    glEnd();
}

@end
