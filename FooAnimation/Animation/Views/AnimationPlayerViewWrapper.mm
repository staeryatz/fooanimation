//
//  AnimationPlayerViewWrapper.cpp
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-24.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#include "AnimationPlayerViewWrapper.hpp"

namespace animation {
	namespace Views {

AnimationPlayerViewWrapper::AnimationPlayerViewWrapper(id<AnimationPlayerViewProtocol> underlyingView)
{
	m_underlyingView = underlyingView;
}

AnimationPlayerViewWrapper::~AnimationPlayerViewWrapper()
{
	m_underlyingView = nil;
}

void AnimationPlayerViewWrapper::registerPlayPressedHandler(CommandHandler handler)
{
	[m_underlyingView registerPlayPressedHandler:^(){
		handler();
	}];
}

void AnimationPlayerViewWrapper::registerPausePressedHandler(CommandHandler handler)
{
	[m_underlyingView registerPausePressedHandler:^(){
		handler();
	}];
}

void AnimationPlayerViewWrapper::registerStopPressedHandler(CommandHandler handler)
{
	[m_underlyingView registerStopPressedHandler:^(){
		handler();
	}];
}

void AnimationPlayerViewWrapper::registerSelectAnimationHandler(SelectAnimationHandler handler)
{
	[m_underlyingView registerSelectAnimationHandler:^(uint16_t animationIndex){
		handler(animationIndex);
	}];
}

void AnimationPlayerViewWrapper::animationPlaybackEnded()
{
	[m_underlyingView animationPlaybackEnded];
}

} // namespace Views
} // namespace animation
