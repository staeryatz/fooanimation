//
//  UiAnimationView.m
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-03.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#import "UiAnimationView.h"

#import "AnimationTypes.hpp"

using namespace animation::Model;

@implementation UiAnimationView

- (void)loadScene:(ScenePtr)scene {
	_scene = scene;
}

- (void)updateFrame:(AnimationFramePtr)frame {
	_currentFrame = frame;
	[self setNeedsDisplayInRect:self.bounds];
}

- (void)displayText:(NSString*)text {
	_text = text;
	[self setNeedsDisplay:YES];
}

- (NSColor*)convertColor:(Color&)color {

	auto red = (CGFloat)color.R / 255;
	auto green = (CGFloat)color.G / 255;
	auto blue = (CGFloat)color.B / 255;
	auto alpha = (CGFloat)color.A / 255;

//	NSLog(@"NSColor - red:%f green:%f blue:%f", red, green, blue);
	return [NSColor colorWithRed:red green:green blue:blue alpha:alpha];
}

- (void)drawRect:(NSRect)dirtyRect {

	if (!_currentFrame)
	{
		[[NSColor blackColor] setFill];
		NSRectFill(dirtyRect);
		[self drawText];
		return;
	}

	auto bgColor = [self convertColor:_currentFrame->backgroundColor];
	[bgColor setFill];

    NSRectFill(dirtyRect);
    [super drawRect:dirtyRect];

	for (auto& sprite : _currentFrame->spriteStates)
	{
		auto scaled = [self scaleToCoordinateSystem:sprite.location.X andY:sprite.location.Y];
		[self drawSprite:sprite atScaledOffset:scaled];
	}

	[self drawText];
}

- (void)drawSprite:(SpriteState)spriteState atScaledOffset:(CGPoint)point {

	if (!_scene)
	{
		NSLog(@"Scene is not initialized.");
		return;
	}

	if (_scene->sprites.count(spriteState.spriteId) < 1)
	{
		NSLog(@"Invalid spriteId: %ld", spriteState.spriteId);
		return;
	}

	[[self convertColor:spriteState.color] setFill];

	auto spriteModel = _scene->sprites[spriteState.spriteId];
	auto spriteSize = spriteModel.size * spriteState.scale;

	NSRect spriteBounds = NSMakeRect(
		point.x - spriteSize / 2,
		point.y - spriteSize / 2,
		spriteSize, spriteSize);

	switch (spriteModel.shape)
	{
		case SpriteShape::Rectangle:
		{
			NSRectFill(spriteBounds);
			break;
		}
		case SpriteShape::Triangle:
		{
			NSBezierPath *trianglePath = [NSBezierPath bezierPath];
			[trianglePath moveToPoint:NSMakePoint(point.x, point.y)];
			[trianglePath lineToPoint:NSMakePoint(point.x + spriteSize / 2, point.y + spriteSize)];
			[trianglePath lineToPoint:NSMakePoint(point.x + spriteSize, point.y)];
			[trianglePath closePath];
			[trianglePath fill];
			break;
		}
		case SpriteShape::Circle:
		{
			NSBezierPath* circularPath = [NSBezierPath bezierPath];
			[circularPath appendBezierPathWithOvalInRect:spriteBounds];
			[circularPath fill];
			break;
		}
		default:
		{
			NSRectFill(spriteBounds);
		}
	}
}

- (void)drawText {

	if (_text == nil || [_text length] == 0)
	{
		return;
	}

	NSDictionary* attributes = [NSDictionary dictionaryWithObjectsAndKeys:
		[NSFont fontWithName:@"Helvetica" size:24], NSFontAttributeName,
		[NSColor whiteColor], NSForegroundColorAttributeName, nil];

	NSAttributedString* drawText = [[NSAttributedString alloc] initWithString:_text attributes: attributes];

	[drawText drawAtPoint:NSMakePoint(12, 12)];
}

- (CGPoint)scaleToCoordinateSystem:(CGFloat)x andY:(CGFloat)y {

	const CGFloat sceneHeight = 20.0;
	const CGFloat sceneWidth = 20.0;

	auto viewHeight = self.bounds.size.height;
	auto viewWidth = self.bounds.size.width;

	auto widthScale = viewWidth / sceneWidth;
	auto heightScale = viewHeight / sceneHeight;

	// Re-center and scale
	CGFloat scaledX = ((viewWidth / 2) + x * (widthScale) * (sceneWidth / 2));
	CGFloat scaledY = ((viewHeight / 2) + y * (heightScale) * (sceneWidth / 2));

	return CGPointMake(scaledX, scaledY);
}

@end
