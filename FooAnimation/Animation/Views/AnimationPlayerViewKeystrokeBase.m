//
//  AnimationPlayerViewKeystrokeBase.m
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-06.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#import "AnimationPlayerViewKeystrokeBase.h"

@implementation AnimationPlayerViewKeystrokeBase

- (void)registerPlayPressedHandler:(CommandHandlerBlock)handler {
	_playCommandHandler = handler;
}

- (void)registerPausePressedHandler:(CommandHandlerBlock)handler {
	_pauseCommandHandler = handler;
}

- (void)registerStopPressedHandler:(CommandHandlerBlock)handler {
	_stopCommandHandler = handler;
}

- (void)registerSelectAnimationHandler:(SelectAnimationHandlerBlock)handler {
	_selectAnimationHandler = handler;
}

- (void)animationPlaybackEnded {
	m_isPlaying = false;
}

// NSView overrides
-(void)keyUp:(NSEvent*)event {
//	NSLog(@"Key down: %d", event.keyCode);
}

-(void)keyDown:(NSEvent*)event {
//	NSLog(@"Key up: %d", event.keyCode);

	const UInt16 KeyCodeChar1 = 18;
	const UInt16 KeyCodeChar2 = 19;
	const UInt16 KeyCodeChar3 = 20;
	const UInt16 KeyCodeCharP = 35;
	const UInt16 KeyCodeCharS = 1;
	const UInt16 KeyCodeCharSpace = 49;

	switch (event.keyCode)
	{
	case KeyCodeChar1:
	case KeyCodeChar2:
	case KeyCodeChar3:
		_selectAnimationHandler(event.keyCode - 18);
		break;
	case KeyCodeCharP:
	case KeyCodeCharSpace:
		if (m_isPlaying)
		{
			_pauseCommandHandler();
		}
		else
		{
			_playCommandHandler();
		}
		m_isPlaying = !m_isPlaying;
		break;
	case KeyCodeCharS:
		_stopCommandHandler();
		break;
	}
}

- (BOOL)acceptsFirstResponder {
    return YES;
}
// end NSView overrides
@end
