//
//  AggregateCannedAnimationLoader.hpp
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-24.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#ifndef AggregateCannedAnimationLoader_hpp
#define AggregateCannedAnimationLoader_hpp

#include "IAnimationLoader.hpp"

#include <memory>

using namespace animation::Model;
using namespace animation::Interfaces;

namespace animation {
	namespace Implementation {
		namespace DataSources {

class AggregateCannedAnimationLoader : public IAnimationLoader
{
public:
	AggregateCannedAnimationLoader(
		std::shared_ptr<IAnimationLoader> loader1,
		std::shared_ptr<IAnimationLoader> loader2,
		std::shared_ptr<IAnimationLoader> loader3);

	~AggregateCannedAnimationLoader() = default;
	std::shared_ptr<Scene> getScene(uint16_t id) override;
	std::vector<Animation> getAnimations(uint16_t id) override;

private:
	std::shared_ptr<IAnimationLoader> m_loader1;
	std::shared_ptr<IAnimationLoader> m_loader2;
	std::shared_ptr<IAnimationLoader> m_loader3;
};

} // namespace DataSources
} // namespace Implementation
} // namespace animation

#endif /* AggregateCannedAnimationLoader_hpp */
