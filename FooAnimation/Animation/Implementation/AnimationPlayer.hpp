//
//  AnimationPlayer.hpp
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-04.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#ifndef AnimationPlayer_hpp
#define AnimationPlayer_hpp

#include "IAnimationPlayer.hpp"

#include "AnimationTypes.hpp"
#include "IKeyframeInterpolator.hpp"
#include "ILogger.hpp"
#include "IPlaybackTimer.hpp"

#include <memory>

using namespace animation::Interfaces;
using namespace logging::Interfaces;

namespace animation {
	namespace Implementation {

class AnimationPlayer : public IAnimationPlayer
{
public:
	AnimationPlayer(
		std::shared_ptr<ILogger> logger,
		std::shared_ptr<IPlaybackTimer> playbackTimer,
		std::shared_ptr<IKeyframeInterpolator> interpolator);
	~AnimationPlayer();

	void registerFrameCallback(FrameCallback callback) override;
	void registerPlaybackEndedCallback(PlaybackEnded callback) override;
	void selectAnimation(Animation& animation) override;

	void play() override;
	void pause() override;
	void stop() override;

private:
	std::shared_ptr<ILogger> m_logger;
	std::shared_ptr<IPlaybackTimer> m_timer;
	std::shared_ptr<IKeyframeInterpolator> m_interpolator;

	FrameCallback m_frameCallback;
	PlaybackEnded m_playbackEndedCallback;

	Animation m_animation;
};

} // namespace Implementation
} // namespace animation

#endif /* AnimationPlayer_hpp */
