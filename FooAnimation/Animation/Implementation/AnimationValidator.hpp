//
//  AnimationValidator.hpp
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-27.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#ifndef AnimationValidator_hpp
#define AnimationValidator_hpp

#include "IAnimationValidator.hpp"

#include "AnimationTypes.hpp"
#include "ILogger.hpp"

#include <memory>

using namespace logging::Interfaces;

namespace animation {
	namespace Implementation {

class AnimationValidator : public animation::Interfaces::IAnimationValidator
{
public:
	AnimationValidator(std::shared_ptr<ILogger> logger);
	~AnimationValidator() = default;
	bool validate(Model::Animation& animation) override;

private:
	bool hasAtLeastTwoKeyframes(Model::Animation& animation);
	bool hasFirstKeyframeAtTimestampZero(Model::Animation& animation);
	bool hasSequentialKeyframeTimestamps(Model::Animation& animation);

	std::shared_ptr<ILogger> m_logger;
};

} // namespace Implementation
} // namespace animation

#endif /* AnimationValidator_hpp */
