//
//  AnimationPlayerViewProtocol.h
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-24.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#ifndef AnimationPlayerViewProtocol_h
#define AnimationPlayerViewProtocol_h

#import <Foundation/Foundation.h>

typedef void (^CommandHandlerBlock)(void);
typedef void (^SelectAnimationHandlerBlock)(uint16_t);

@protocol AnimationPlayerViewProtocol <NSObject>

- (void)registerPlayPressedHandler:(CommandHandlerBlock)handler;
- (void)registerPausePressedHandler:(CommandHandlerBlock)handler;
- (void)registerStopPressedHandler:(CommandHandlerBlock)handler;
- (void)registerSelectAnimationHandler:(SelectAnimationHandlerBlock)handler;

- (void)animationPlaybackEnded;

@end

#endif /* AnimationPlayerViewProtocol_h */
