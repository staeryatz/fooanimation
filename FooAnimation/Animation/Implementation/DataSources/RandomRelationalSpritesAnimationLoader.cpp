//
//  RandomRelationalSpritesAnimationLoader.cpp
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-03.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#include "RandomRelationalSpritesAnimationLoader.hpp"

#include "AnimationTypes.hpp"

using namespace std;

namespace animation {
	namespace Implementation {
		namespace DataSources {

ScenePtr RandomRelationalSpritesAnimationLoader::getScene(uint16_t id)
{
	auto scene = make_shared<Scene>();

	for (int spriteCount = 1; spriteCount <= 4; spriteCount++)
	{
		Sprite sprite;
		sprite.spriteId = spriteCount;
		sprite.shape = Triangle;
		sprite.size = 32;

		scene->sprites.insert({spriteCount, sprite});
	}
	return scene;
}

std::vector<Animation> RandomRelationalSpritesAnimationLoader::getAnimations(uint16_t id)
{
	Animation animation;
	animation.name = "Random relational sprites";

	// OpenGlAnimationView
	const auto startX = -10;
	const auto startY = -10;
	const auto height = 20;
	const auto width = 20;

	Color backgroundColor;
	backgroundColor.R = 0;
	backgroundColor.G = 0;
	backgroundColor.B = 0;

	for (int i = 0; i < 20; i++)
	{
		auto frame = make_shared<Keyframe>();
		frame->backgroundColor = backgroundColor;

		Color spriteColor;
		spriteColor.R = 0 + arc4random() % (255 - 0 + 1);
		spriteColor.G = 0 + arc4random() % (255 - 0 + 1);
		spriteColor.B = 0 + arc4random() % (255 - 0 + 1);

		frame->timestamp = DurationInSeconds(i * 0.5);
//		NSLog(@"Creating keyframe at seconds: %f", frame.timestamp.count());

		auto posX = (double)(1 + arc4random() % (width - 0 + -1)) + startX;
		auto posY = (double)(1 + arc4random() % (height - 0 + -1)) + startY;
		auto posZ = (double)1.0;

		SpriteState sprite1;
		sprite1.spriteId = 1;
		sprite1.location.X = -posX * 0.1;
		sprite1.location.Y = posY * 0.1;
		sprite1.location.Z = posZ;
		sprite1.color = spriteColor;

		SpriteState sprite2;
		sprite2.spriteId = 2;
		sprite2.location.X = posX * 0.1;
		sprite2.location.Y = -posY * 0.1;
		sprite2.location.Z = posZ;
		sprite2.color.R = spriteColor.G;
		sprite2.color.G = spriteColor.R;
		sprite2.color.B = spriteColor.B;

		SpriteState sprite3;
		sprite3.spriteId = 3;
		sprite3.location.X = sprite2.location.X - sprite1.location.Y;
		sprite3.location.Y = sprite2.location.Y - sprite1.location.X;
		sprite3.location.Z = posZ;
		sprite3.color.R = spriteColor.B;
		sprite3.color.G = spriteColor.G;
		sprite3.color.B = spriteColor.R;

		SpriteState sprite4;
		sprite4.spriteId = 4;
		sprite4.location.X = sprite1.location.X - sprite2.location.Y;
		sprite4.location.Y = sprite2.location.Y - sprite1.location.X;
		sprite4.location.Z = posZ;
		sprite4.color.R = spriteColor.R;
		sprite4.color.G = spriteColor.B;
		sprite4.color.B = spriteColor.G;

		frame->spriteStates.push_back(sprite1);
		frame->spriteStates.push_back(sprite2);
		frame->spriteStates.push_back(sprite3);
		frame->spriteStates.push_back(sprite4);

		animation.keyframes.push_back(frame);
	}

//	auto animationSize = animation.keyframes.size();
//	NSLog(@"Loaded %f seconds long animation with %d keyframes",
//		animationSize, animation.keyframes[animationSize -1].timestamp.count());

	return std::vector<Animation> {animation};
}

} // namespace DataSources
} // namespace Implementation
} // namespace animation
