//
//  IAnimationPlayer.h
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-04.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#ifndef IAnimationPlayer_h
#define IAnimationPlayer_h

#include "AnimationTypes.hpp"

#include <functional>

using namespace animation::Model;

namespace animation {
	namespace Interfaces {

using FrameCallback = std::function<void(AnimationFramePtr)>;
using PlaybackEnded = std::function<void()>;

class IAnimationPlayer
{
public:
	virtual ~IAnimationPlayer() = default;
	virtual void registerFrameCallback(FrameCallback callback) = 0;
	virtual void registerPlaybackEndedCallback(PlaybackEnded callback) = 0;
	virtual void selectAnimation(Animation& animation) = 0;

	virtual void play() = 0;
	virtual void pause() = 0;
	virtual void stop() = 0;
};

} // namespace Interfaces {
} // namespace animation

#endif /* IAnimationPlayer_h */
