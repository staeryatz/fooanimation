//
//  Logger.hpp
//  Logging
//
//  Created by Jeffrey Bakker on 2021-12-26.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#pragma GCC visibility push(default)

#include "ILogger.hpp"

namespace logging {
	namespace Implementation {

class Logger : public logging::Interfaces::ILogger
{
public:
	~Logger() = default;
    void log(std::string message) override;
};

} // namesapce Implementation
} // namespace logging

#pragma GCC visibility pop
