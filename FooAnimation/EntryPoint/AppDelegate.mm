//
//  AppDelegate.m
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-01.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#import "AppDelegate.h"

#import "UiAnimationView.h"
#import "OpenGlAnimationView.h"

#include "AnimationIocModule.hpp"
#include "AnimationPlayerViewWrapper.hpp"
#include "AnimationViewWrapper.hpp"
#include "IAnimationController.hpp"
#include "LoggingIocModule.hpp"

#include "ServiceLocator.hpp"

#include <memory>

using namespace animation::InversionOfControl;
using namespace logging::InversionOfControl;
using namespace std;

@interface AppDelegate () {

	shared_ptr<IAnimationController> m_animationController;
}

@property (weak) IBOutlet NSWindow *window;
@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {

	[self setupAnimationView];
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {

	m_animationController = nullptr;
}

- (void)setupAnimationView {

//	NSView<AnimationViewProtocol>* animationView = [[OpenGlAnimationView alloc] initWithFrame:_window.frame];
	NSView<AnimationViewProtocol>* animationView = [[UiAnimationView alloc] initWithFrame:_window.frame];
	_window.contentView = animationView;
	[_window makeFirstResponder:animationView];

	id<AnimationPlayerViewProtocol> playerView = (id<AnimationPlayerViewProtocol>)animationView;

	auto iocContainer = ServiceLocator::create();
	iocContainer->modules().add<AnimationIocModule>();
	iocContainer->modules().add<LoggingIocModule>();

	shared_ptr<IAnimationView> animationViewWrapper = make_shared<AnimationViewWrapper>(animationView);
	shared_ptr<IAnimationPlayerView> animationPlayerViewWrapper = make_shared<AnimationPlayerViewWrapper>(playerView);

	iocContainer->bind<IAnimationView>().toInstance(animationViewWrapper);
	iocContainer->bind<IAnimationPlayerView>().toInstance(animationPlayerViewWrapper);

	auto iocContext = iocContainer->getContext();
	m_animationController = iocContext->resolve<IAnimationController>();
}

@end
