//
//  IAnimationController.hpp
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-24.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#ifndef IAnimationController_h
#define IAnimationController_h

namespace animation {
	namespace Interfaces {

class IAnimationController
{
public:
	virtual ~IAnimationController() = default;
};

} // namespace Interfaces
} // namespace animation

#endif /* IAnimationController_h */
