//
//  AnimationController.cpp
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-24.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#include "AnimationController.hpp"

using namespace std;

namespace animation {
	namespace Implementation {

AnimationController::AnimationController(
	shared_ptr<IAnimationValidator> validator,
	shared_ptr<IAnimationLoader> loader, shared_ptr<IAnimationPlayer> player,
	shared_ptr<IAnimationPlayerView> playerView, shared_ptr<IAnimationView> view)
	: m_validator(validator)
	, m_loader(loader)
	, m_player(player)
	, m_playerView(playerView)
	, m_view(view)
{
	setupCallbacks();

	m_view->displayText("Press 1, 2, or 3 to load animation");
}

void AnimationController::setupCallbacks()
{
	m_playerView->registerPlayPressedHandler([this]{
		if (m_animationLoaded)
		{
			m_player->play();
		}
	});

	m_playerView->registerPausePressedHandler([this]{
		if (m_animationLoaded)
		{
			m_player->pause();
		}
	});

	m_playerView->registerStopPressedHandler([this]{
		if (m_animationLoaded)
		{
			m_player->stop();
		}
	});

	m_playerView->registerSelectAnimationHandler([this](uint16_t animationIndex) {
		loadSceneIntoView(animationIndex);
		selectAnimation(animationIndex);

		m_animationLoaded = true;
	});

	m_player->registerFrameCallback([this](AnimationFramePtr frame) {
		m_view->updateFrame(frame);
	});

	m_player->registerPlaybackEndedCallback([this]() {
		m_playerView->animationPlaybackEnded();
	});
}

void AnimationController::loadSceneIntoView(uint16_t animationIndex)
{
	auto scene = m_loader->getScene(animationIndex);

	m_view->loadScene(scene);
}

void AnimationController::selectAnimation(uint16_t animationIndex)
{
	auto animations = m_loader->getAnimations(animationIndex);

	if (animations.size() > 0)
	{
		auto animation = animations[0];
		if (!m_validator->validate(animation))
		{
			return;
		}

		requestAnimationPreview(animation);
		m_player->selectAnimation(animation);
	}
}

void AnimationController::requestAnimationPreview(Animation& animation)
{
	if (animation.keyframes.size() == 0)
	{
		return;
	}

	auto firstFrame = animation.keyframes[0];
	m_view->displayText(animation.name);
	m_view->updateFrame(firstFrame);
}

} // namespace Implementation
} // namespace animation
