//
//  RandomSwarmSpritesAnimationLoader.cpp
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-19.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#include "RandomSwarmSpritesAnimationLoader.hpp"

#include "AnimationTypes.hpp"

#include <iostream>

using namespace std;

namespace animation {
	namespace Implementation {
		namespace DataSources {

ScenePtr RandomSwarmSpritesAnimationLoader::getScene(uint16_t id)
{
	auto scene = make_shared<Scene>();

	for (int spriteCount = 0; spriteCount < 255; spriteCount++)
	{
		Sprite sprite;
		sprite.spriteId = spriteCount;
		sprite.shape = Circle;
		sprite.size = 24;

		scene->sprites.insert({spriteCount, sprite});
	}
	return scene;
}

vector<Animation> RandomSwarmSpritesAnimationLoader::getAnimations(uint16_t id)
{
	Animation animation;
	animation.name = "Random swarm sprites";

	// OpenGlAnimationView
	const auto startX = -10;
	const auto startY = -10;
	const auto height = 20;
	const auto width = 20;

	Color backgroundColor;
	backgroundColor.R = 0;
	backgroundColor.G = 0;
	backgroundColor.B = 0;

	for (int frames = 0; frames < 511; frames++)
	{
		auto frame = make_shared<Keyframe>();
		frame->backgroundColor.R = frames / 2;
		frame->backgroundColor.G = frames / 2;
		frame->backgroundColor.B = frames / 2;

		Color spriteColor;
		spriteColor.R = 0 + arc4random() % (255 - 0 + 1);
		spriteColor.G = 0 + arc4random() % (255 - 0 + 1);
		spriteColor.B = 0 + arc4random() % (255 - 0 + 1);
		spriteColor.A = 192;

		SpriteState sprite;

		for (int sprites = 0; sprites < 255; sprites++)
		{
			auto scale = (double)(50 + arc4random() % (100 - 50 + 1)) * 0.01;

			auto posX = (double)(1 + arc4random() % (width - 0 + -1)) + startX;
			auto posY = (double)(1 + arc4random() % (height - 0 + -1)) + startY;
			auto posZ = (double)1.0;

			sprite.spriteId = sprites;
			sprite.location.X = -posX * 0.1;
			sprite.location.Y = posY * 0.1;
			sprite.location.Z = posZ;

			sprite.color = spriteColor;
			sprite.scale = scale;

			frame->spriteStates.push_back(sprite);
		}

		frame->timestamp = DurationInSeconds(1.0 * frames);
		animation.keyframes.push_back(frame);
	}

//	auto animationSize = animation.keyframes.size();
//	NSLog(@"Loaded %f seconds long animation with %d keyframes",
//		animationSize, animation.keyframes[animationSize -1].timestamp.count());

	return vector<Animation> {animation};
}

} // namespace DataSources
} // namespace Implementation
} // namespace animation
