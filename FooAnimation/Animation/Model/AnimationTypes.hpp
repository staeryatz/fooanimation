//
//  AnimationTypes.h
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-03.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#ifndef AnimationTypes_h
#define AnimationTypes_h

#include <chrono>
#include <map>
#include <memory>
#include <string>
#include <vector>

namespace animation {
	namespace Model {

struct AnimationFrame;
struct Keyframe;
struct Scene;

using AnimationFramePtr = std::shared_ptr<AnimationFrame>;
using KeyframePtr = std::shared_ptr<Keyframe>;
using ScenePtr = std::shared_ptr<Scene>;

using DurationInSeconds = std::chrono::duration<float, std::ratio<1, 1>>;

enum SpriteShape
{
	Rectangle,
	Triangle,
	Circle
};

struct Color
{
    uint8_t R;
    uint8_t G;
    uint8_t B;
    uint8_t A{255};
};

struct Location
{
	float X;
	float Y;
	float Z;
};

struct Sprite
{
	long spriteId;
	SpriteShape shape{Rectangle};
	float size;
};

struct Scene
{
	std::map<long, Sprite> sprites;
};

struct SpriteState
{
	long spriteId;
	Location location;
	Color color;
	float scale{1.0};
	bool visible{true};
};

struct AnimationFrame
{
	std::vector<SpriteState> spriteStates;
	Color backgroundColor;

	virtual bool isKey() { return false; }
};

struct Keyframe : public AnimationFrame
{
	DurationInSeconds timestamp;

	bool isKey() override { return true; }
};

struct Animation
{
	long animationId;
	std::string name;
	std::vector<std::shared_ptr<Keyframe>> keyframes;
};

} // namespace Model
} // namespace animation

#endif /* AnimationTypes_h */
