//
//  OpenGlAnimationView.h
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-03.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#pragma once

#import "AnimationViewProtocol.h"

#import "AnimationTypes.hpp"
#import "AnimationPlayerViewProtocol.h"

#import <Cocoa/Cocoa.h>
#define GL_SILENCE_DEPRECATION
#include <OpenGL/gl.h>

//NS_ASSUME_NONNULL_BEGIN

@interface OpenGlAnimationView : NSOpenGLView<AnimationViewProtocol, AnimationPlayerViewProtocol> {
	bool m_isPlaying;
}

// IAnimationPlayerView conformance
@property CommandHandlerBlock playCommandHandler;
@property CommandHandlerBlock pauseCommandHandler;
@property CommandHandlerBlock stopCommandHandler;
@property SelectAnimationHandlerBlock selectAnimationHandler;

- (void)registerPlayPressedHandler:(CommandHandlerBlock)handler;
- (void)registerPausePressedHandler:(CommandHandlerBlock)handler;
- (void)registerStopPressedHandler:(CommandHandlerBlock)handler;
- (void)registerSelectAnimationHandler:(SelectAnimationHandlerBlock)handler;

- (void)animationPlaybackEnded;
// end IAnimationPlayerView

// IAnimationView
@property ScenePtr scene;
@property AnimationFramePtr currentFrame;
@property NSString* text;

- (void)loadScene:(ScenePtr)scene;
- (void)updateFrame:(AnimationFramePtr)frame;
- (void)displayText:(NSString*)text;

- (void)drawSprite:(SpriteState)spriteState;
- (NSColor*)convertColor:(animation::Model::Color&)color;
// end IAnimationView

@end

//NS_ASSUME_NONNULL_END
