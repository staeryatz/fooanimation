//
//  AnimationViewWrapper.hpp
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-24.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#ifndef AnimationView_hpp
#define AnimationView_hpp

#include "IAnimationView.hpp"

#include "AnimationViewProtocol.h"

namespace animation {
	namespace Views {

class AnimationViewWrapper : public animation::Interfaces::IAnimationView
{
public:
	AnimationViewWrapper(id<AnimationViewProtocol> underlyingView);
	~AnimationViewWrapper();
	void loadScene(ScenePtr scene) override;
	void updateFrame(AnimationFramePtr frame) override;
	void displayText(std::string text) override;

private:
	id<AnimationViewProtocol> m_underlyingView;
};

} // namespace Views
} // namespace animation

#endif /* AnimationView_hpp */
