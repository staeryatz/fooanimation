//
//  KeyframeInterpolator.hpp
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-03.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#ifndef KeyframeInterpolator_hpp
#define KeyframeInterpolator_hpp

#include "IKeyframeInterpolator.hpp"

namespace animation {
	namespace Implementation {

using namespace Interfaces;

class KeyframeInterpolator : public IKeyframeInterpolator
{
public:
	AnimationFramePtr interpolate(
		const KeyframePtr last, const KeyframePtr next,
		const DurationInSeconds currTime) override;

private:
	Location interpolate(const Location& lastLocation, const Location& nextLocation, const float& scale);
	Color interpolate(const Color& lastColor, const Color& nextColor, const float& scale);
	uint8_t interpolate(const uint8_t& last, const uint8_t& next, const float& scale);
	float interpolate(const float& last, const float& next, const float& scale);
};

} // namespace Implementation
} // namespace animation
#endif /* KeyframeInterpolator_hpp */
