//
//  RandomSwarmSpritesAnimationLoader.hpp
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-19.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#ifndef RandomSwarmSpritesAnimationLoader_hpp
#define RandomSwarmSpritesAnimationLoader_hpp

#include "IAnimationLoader.hpp"

using namespace animation::Model;
using namespace animation::Interfaces;

namespace animation {
	namespace Implementation {
		namespace DataSources {

class RandomSwarmSpritesAnimationLoader : public IAnimationLoader
{
public:
	~RandomSwarmSpritesAnimationLoader() = default;
	std::shared_ptr<Scene> getScene(uint16_t id) override;
	std::vector<Animation> getAnimations(uint16_t id) override;
};

} // namespace DataSources
} // namespace Implementation
} // namespace animation

#endif /* RandomSwarmSpritesAnimationLoader_hpp */
