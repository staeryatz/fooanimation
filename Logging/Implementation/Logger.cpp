//
//  Logger.cpp
//  Logging
//
//  Created by Jeffrey Bakker on 2021-12-26.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#include "Logger.hpp"

#include <fmt/format.h>
#include <fmt/chrono.h>

#include <iostream>

using namespace std;

namespace logging {
	namespace Implementation {

void Logger::log(std::string message)
{
	auto time = std::chrono::system_clock::now();
	cout << fmt::format("{:%Y-%m-%dT%H:%M:%S%0z}: ", time) << message << endl;
};

} // namespace Implementation
} // namespace logging
