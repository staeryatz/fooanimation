//
//  ILogger.hpp
//  Logging
//
//  Created by Jeffrey Bakker on 2021-12-26.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#ifndef ILogger_
#define ILogger_

#include <string>

/* The classes below are exported */
#pragma GCC visibility push(default)

namespace logging {
	namespace Interfaces {

class ILogger
{
public:
	virtual ~ILogger() = default;
    virtual void log(std::string message) = 0;
};

} // namespace Interfaces
} // namespace logging

#pragma GCC visibility pop
#endif
