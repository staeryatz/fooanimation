//
//  PlaybackTimerTests.m
//  FooAnimationTests
//
//  Created by Jeffrey Bakker on 2021-12-20.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#import "PlaybackTimer.hpp"

#import <XCTest/XCTest.h>

#import <memory>

using namespace animation::Implementation;
using namespace std;

@interface PlaybackTimerTests : XCTestCase {
	unique_ptr<PlaybackTimer> m_uut;
}

@end

@implementation PlaybackTimerTests

- (void)setUp {
	m_uut = make_unique<PlaybackTimer>(0.0002);
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

- (void)testTimerFiredCallback {

	auto expectation = [[XCTestExpectation alloc] initWithDescription:@"Timer interval fired"];

	m_uut->registerTimerIntervalFired(^(TimerPlaybackInfo playbackInfo) {
		[expectation fulfill];
	});

	m_uut->start();

	[self waitForExpectations:@[expectation] timeout:0.0003];
}

- (void)testPause {

	auto expectation = [[XCTestExpectation alloc] initWithDescription:@"Timer interval fired"];
	[expectation setInverted:YES];

	m_uut->registerTimerIntervalFired(^(TimerPlaybackInfo playbackInfo) {
		[expectation fulfill];
	});

	m_uut->start();
	m_uut->pause();

	[self waitForExpectations:@[expectation] timeout:0.0003];
}

- (void)testResume {

	auto expectationPause = [[XCTestExpectation alloc] initWithDescription:@"Timer interval can't fire while paused"];
	[expectationPause setInverted:YES];

	auto expectationResume = [[XCTestExpectation alloc] initWithDescription:@"Timer interval can fire after resume"];

	__block const auto pausedTime = 0.005;
	__block auto firstCallback = true;
	m_uut->registerTimerIntervalFired(^(TimerPlaybackInfo playbackInfo) {
		[expectationPause fulfill];
		[expectationResume fulfill];

		if (firstCallback)
		{
			const auto actualElapsed = playbackInfo.elapsed.count();

			XCTAssertLessThan(actualElapsed, pausedTime);
			firstCallback = false;
		}
	});

	m_uut->start();
	m_uut->pause();

	[self waitForExpectations:@[expectationPause] timeout:pausedTime];

	m_uut->resume();

	[self waitForExpectations:@[expectationResume] timeout:0.0003];
}

- (void)testStopWhenPausedNoCrash {

	m_uut->registerTimerIntervalFired(^(TimerPlaybackInfo playbackInfo) {
	});

	m_uut->start();
	m_uut->pause();
	m_uut->stop();
}

- (void)testTimerEndedCallback {

	__block auto timerEndedFired = false;
	m_uut->registerTimerEnded(^{
		timerEndedFired = true;
	});

	m_uut->start();
	m_uut->stop();

	XCTAssertTrue(timerEndedFired);
}

- (void)testTimerWontEndIfNotStarted {

	__block auto timerEndedFired = false;
	m_uut->registerTimerEnded(^{
		timerEndedFired = true;
	});

	m_uut->stop();

	XCTAssertFalse(timerEndedFired);
}

@end
