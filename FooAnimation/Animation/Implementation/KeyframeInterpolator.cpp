//
//  KeyframeInterpolator.cpp
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-03.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#include "KeyframeInterpolator.hpp"

#include "AnimationTypes.hpp"

#include <cmath>

using namespace std;

namespace animation {
	namespace Implementation {

AnimationFramePtr KeyframeInterpolator::interpolate(
	const KeyframePtr last, const KeyframePtr next, const DurationInSeconds currTime)
{
	auto tweenFrame = make_shared<AnimationFrame>();

	auto lastTime = last->timestamp;
	auto nextTime = next->timestamp;

	if (currTime >= nextTime)
	{
		return next;
	}

	DurationInSeconds differenceScale = nextTime - lastTime;
	DurationInSeconds currScale = currTime - lastTime;

	float scaleBy = currScale.count() / differenceScale.count();
//	NSLog(@"Interpolation scale by: %f from %f - %f", scaleBy, currTime.count(), lastTime.count());

	size_t lastSpriteCount = last->spriteStates.size();
	size_t nextSpriteCount = next->spriteStates.size();

	size_t higherSpriteCount = fmax(lastSpriteCount, nextSpriteCount);

	for (size_t i = 0; i < higherSpriteCount; i++)
	{
		if (i < lastSpriteCount && i < nextSpriteCount)
		{
			auto lastSprite = last->spriteStates[i];
			auto nextSprite = next->spriteStates[i];

			SpriteState tweenSprite;
			tweenSprite.spriteId = lastSprite.spriteId;
			tweenSprite.visible = lastSprite.visible;
			tweenSprite.location = interpolate(lastSprite.location, nextSprite.location, scaleBy);;
			tweenSprite.color = interpolate(lastSprite.color, nextSprite.color, scaleBy);
			tweenSprite.scale = interpolate(lastSprite.scale, nextSprite.scale, scaleBy);

			tweenFrame->spriteStates.push_back(tweenSprite);
		}
	}

	tweenFrame->backgroundColor = interpolate(last->backgroundColor, next->backgroundColor, scaleBy);

	return tweenFrame;
}

Location KeyframeInterpolator::interpolate(const Location& lastLocation, const Location& nextLocation, const float& scale)
{
	Location interpolated;
	interpolated.X = lastLocation.X + (nextLocation.X - lastLocation.X) * scale;
	interpolated.Y = lastLocation.Y + (nextLocation.Y - lastLocation.Y) * scale;
	interpolated.Z = lastLocation.Z + (nextLocation.Z - lastLocation.Z) * scale;

//	NSLog(@"Last Position - X:%f Y:%f Z:%f", lastLocation.X, lastLocation.Y, lastLocation.Z);
//	NSLog(@"Next Position - X:%f Y:%f Z:%f", nextLocation.X, nextLocation.Y, nextLocation.Z);
//	NSLog(@"Tween Position - X:%f Y:%f Z:%f", interpolated.X, interpolated.Y, interpolated.Z);

	return interpolated;
}

Color KeyframeInterpolator::interpolate(const Color& lastColor, const Color& nextColor, const float& scale)
{
	Color interpolated;
	interpolated.R = interpolate(lastColor.R, nextColor.R, scale);
	interpolated.G = interpolate(lastColor.G, nextColor.G, scale);
	interpolated.B = interpolate(lastColor.B, nextColor.B, scale);
	interpolated.A = interpolate(lastColor.A, nextColor.A, scale);

	return interpolated;
}

uint8_t KeyframeInterpolator::interpolate(const uint8_t& last, const uint8_t& next, const float& scale)
{
	if (last < next)
	{
		return last + (next - last) * scale;
	}
	else if (last > next)
	{
		return next + (last - next) * scale;
	}
	else
	{
		return next;
	}
}

float KeyframeInterpolator::interpolate(const float& last, const float& next, const float& scale)
{
	if (last < next)
	{
		return last + (next - last) * scale;
	}
	else if (last > next)
	{
		return next + (last - next) * scale;
	}
	else
	{
		return next;
	}
}

} // namespace Implementation
} // namespace animation
