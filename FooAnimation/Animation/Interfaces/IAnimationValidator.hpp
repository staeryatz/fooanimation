//
//  IAnimationValidator.hpp
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-27.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#ifndef IAnimationValidator_h
#define IAnimationValidator_h

#include "AnimationTypes.hpp"

namespace animation {
	namespace Interfaces {

class IAnimationValidator
{
public:
	virtual ~IAnimationValidator() = default;
	virtual bool validate(Model::Animation& animation) = 0;
};

} // namespace Interfaces
} // namespace animation

#endif /* IAnimationValidator_h */
