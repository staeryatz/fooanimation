//
//  IncrementalSpritesAnimationLoader.hpp
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-05.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#ifndef IncrementalSpritesAnimationLoader_hpp
#define IncrementalSpritesAnimationLoader_hpp

#include "IAnimationLoader.hpp"

using namespace animation::Model;
using namespace animation::Interfaces;

namespace animation {
	namespace Implementation {
		namespace DataSources {

class IncrementalSpritesAnimationLoader : public IAnimationLoader
{
public:
	~IncrementalSpritesAnimationLoader() = default;
	std::shared_ptr<Scene> getScene(uint16_t id) override;
	std::vector<Animation> getAnimations(uint16_t id) override;
};

} // namespace DataSources
} // namespace Implementation
} // namespace animation

#endif /* IncrementalSpritesAnimationLoader_hpp */
