//
//  IPlaybackTimer.h
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-15.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#ifndef IPlaybackTimer_h
#define IPlaybackTimer_h

#include "AnimationTypes.hpp"

#include <chrono>

using namespace animation::Model;

namespace animation {
namespace Interfaces {

struct TimerPlaybackInfo
{
	DurationInSeconds elapsed;
};

typedef void (^TimerIntervalFiredCallback)(TimerPlaybackInfo);
typedef void (^TimerEndedCallback)(void);

class IPlaybackTimer
{
public:
	virtual ~IPlaybackTimer() = default;

	virtual void registerTimerIntervalFired(TimerIntervalFiredCallback callback) = 0;
	virtual void registerTimerEnded(TimerEndedCallback callback) = 0;

	virtual void start() = 0;
	virtual void stop() = 0;
	virtual void pause() = 0;
	virtual void resume() = 0;

	virtual bool isActive() = 0;
};

} // namespace Interfaces
} // namespace animation

#endif /* IPlaybackTimer_h */
