//
//  AnimationValidator.cpp
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-27.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#include "AnimationValidator.hpp"

#include "AnimationTypes.hpp"

#include <fmt/core.h>

using namespace animation::Model;
using namespace std;

namespace animation {
	namespace Implementation {

AnimationValidator::AnimationValidator(shared_ptr<ILogger> logger)
	: m_logger(logger)
{
}

bool AnimationValidator::validate(Model::Animation& animation)
{
	m_logger->log(fmt::format("Validating animation '{}'...", animation.name));

	bool result = true;

	if (!hasAtLeastTwoKeyframes(animation))
	{
		m_logger->log("Animations cannot have less than two keyframes!");
		result = false;
	}

	if (!hasFirstKeyframeAtTimestampZero(animation))
	{
		m_logger->log("Animation's first keyframe timestamp is not zero!");
		result = false;
	}

	if (!hasSequentialKeyframeTimestamps(animation))
	{
		m_logger->log("Animation keyframe timestamps are not in sequential order!");
		result = false;
	}

	if (!result)
	{
		m_logger->log("Validation failed");
	}

	return result;
}

bool AnimationValidator::hasAtLeastTwoKeyframes(Animation& animation)
{
	return animation.keyframes.size() >= 2;
}

bool AnimationValidator::hasFirstKeyframeAtTimestampZero(Model::Animation& animation)
{
	DurationInSeconds zero{0.0f};
	DurationInSeconds firstFrameTimestamp = animation.keyframes[0]->timestamp;

	m_logger->log(fmt::format("First keyframe is at {:f}s", firstFrameTimestamp.count()));

	return (zero.count() == firstFrameTimestamp.count());
}

bool AnimationValidator::hasSequentialKeyframeTimestamps(Model::Animation& animation)
{
	size_t lastKeyframeIndex = 0;
	size_t keyframeIndex = 1;

	bool result = true;

	for (; keyframeIndex <= animation.keyframes.size() - 1; keyframeIndex++)
	{
    	const auto currentFrameTimestamp = animation.keyframes[keyframeIndex]->timestamp;
    	const auto lastFrameTimestamp = animation.keyframes[lastKeyframeIndex]->timestamp;
		if (currentFrameTimestamp.count() <= lastFrameTimestamp.count())
		{
			m_logger->log(fmt::format(
				"Keyframe{:d} at {:f}s does not precede keyframe{:d} at {:f}s",
				lastKeyframeIndex, lastFrameTimestamp.count(), keyframeIndex, currentFrameTimestamp.count()));

			result = false;
		}
		lastKeyframeIndex++;
	}
	return result;
}

} // namespace Implementation
} // namespace animation
