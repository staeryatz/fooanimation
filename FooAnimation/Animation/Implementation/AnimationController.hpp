//
//  AnimationController.hpp
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-24.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#ifndef AnimationController_hpp
#define AnimationController_hpp

#include "IAnimationController.hpp"

#include "IAnimationLoader.hpp"
#include "IAnimationPlayer.hpp"
#include "IAnimationPlayerView.hpp"
#include "IAnimationValidator.hpp"
#include "IAnimationView.hpp"

#include <memory>

using namespace animation::Interfaces;

namespace animation {
	namespace Implementation {

class AnimationController : public IAnimationController
{
public:
	AnimationController(
		std::shared_ptr<IAnimationValidator> validator,
		std::shared_ptr<IAnimationLoader> loader, std::shared_ptr<IAnimationPlayer> player,
		std::shared_ptr<IAnimationPlayerView> playerView, std::shared_ptr<IAnimationView> view);

	~AnimationController() = default;

private:
	void setupCallbacks();
	void loadSceneIntoView(uint16_t animationIndex);
	void selectAnimation(uint16_t animationIndex);
	void requestAnimationPreview(Animation& animation);

	std::shared_ptr<IAnimationValidator> m_validator;
	std::shared_ptr<IAnimationLoader> m_loader;
	std::shared_ptr<IAnimationPlayer> m_player;
	std::shared_ptr<IAnimationPlayerView> m_playerView;
	std::shared_ptr<IAnimationView> m_view;

	bool m_animationLoaded{false};
};

} // namespace Implementation
} // namespace animation

#endif /* AnimationController_hpp */
