//
//  IncrementalSpritesAnimationLoader.cpp
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-05.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#include "IncrementalSpritesAnimationLoader.hpp"

#include "AnimationTypes.hpp"

using namespace animation::Model;
using namespace std;

namespace animation {
	namespace Implementation {
		namespace DataSources {

ScenePtr IncrementalSpritesAnimationLoader::getScene(uint16_t id)
{
	auto scene = make_shared<Scene>();

	for (int spriteCount = 0; spriteCount < 3; spriteCount++)
	{
		Sprite sprite;
		sprite.spriteId = spriteCount;
		sprite.shape = Rectangle;
		sprite.size = 32;

		scene->sprites.insert({spriteCount, sprite});
	}
	return scene;
}

std::vector<Animation> IncrementalSpritesAnimationLoader::getAnimations(uint16_t id)
{
	Animation animation;
	animation.name = "Incremental sprites";
	Color spriteColor;
	spriteColor.R = 0;
	spriteColor.G = 0;
	spriteColor.B = 0;

	for (int i = 0; i < 20; i++)
	{
		auto frame = make_shared<Keyframe>();
		frame->backgroundColor.R = 15 + i * 12;
		frame->backgroundColor.G = 90 + i * 6;
		frame->backgroundColor.B = 60;

		frame->timestamp = DurationInSeconds(i * 0.5);
//		NSLog(@"Creating keyframe at seconds: %f", frame.timestamp.count());

		auto posX = (double)i;
		auto posY = (double)i;
		auto posZ = (double)1.0;

		SpriteState sprite1;
		sprite1.spriteId = 0;
		sprite1.location.X = -posX * 0.05;
		sprite1.location.Y = posY * 0.05;
		sprite1.location.Z = posZ;
		sprite1.color = spriteColor;
		sprite1.scale = 0.5 + (0.5 / 20) * i;

		SpriteState sprite2;
		sprite2.spriteId = 1;
		sprite2.location.X = posX * 0.05;
		sprite2.location.Y = -posY * 0.05;
		sprite2.location.Z = posZ;
		sprite2.color = spriteColor;
		sprite2.scale = 0.5 + (0.5 / 20) * i;

		SpriteState sprite3;
		sprite3.spriteId = 2;
		sprite3.location.X = 0;
		sprite3.location.Y = 0;
		sprite3.location.Z = posZ;
		sprite3.color = spriteColor;
		sprite3.scale = 1.0;

		frame->spriteStates.push_back(sprite1);
		frame->spriteStates.push_back(sprite2);
		frame->spriteStates.push_back(sprite3);

		animation.keyframes.push_back(move(frame));
	}

//	auto animationSize = animation.keyframes.size();
//	NSLog(@"Loaded %f seconds long animation with %d keyframes",
//		animationSize, animation.keyframes[animationSize -1].timestamp.count());

	return std::vector<Animation> {animation};
}

} // namespace DataSources
} // namespace Implementation
} // namespace animation
