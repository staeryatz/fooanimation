//
//  AnimationPlayerViewWrapper.hpp
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-24.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#ifndef AnimationPlayerViewWrapper_hpp
#define AnimationPlayerViewWrapper_hpp

#include "IAnimationPlayerView.hpp"

#include "AnimationPlayerViewProtocol.h"

namespace animation {
	namespace Views {

class AnimationPlayerViewWrapper : public animation::Interfaces::IAnimationPlayerView
{
public:
	AnimationPlayerViewWrapper(id<AnimationPlayerViewProtocol> underlyingView);
	~AnimationPlayerViewWrapper();
	void registerPlayPressedHandler(CommandHandler handler) override;
	void registerPausePressedHandler(CommandHandler handler) override;
	void registerStopPressedHandler(CommandHandler handler) override;
	void registerSelectAnimationHandler(SelectAnimationHandler handler) override;
	void animationPlaybackEnded() override;

private:
	id<AnimationPlayerViewProtocol> m_underlyingView;
};

} // namespace Views
} // namespace animation

#endif /* AnimationPlayerViewWrapper_hpp */
