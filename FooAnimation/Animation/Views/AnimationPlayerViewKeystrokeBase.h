//
//  AnimationPlayerViewKeystrokeBase.h
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-06.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "AnimationPlayerViewProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface AnimationPlayerViewKeystrokeBase : NSView<AnimationPlayerViewProtocol> {
	bool m_isPlaying;
}

@property CommandHandlerBlock playCommandHandler;
@property CommandHandlerBlock pauseCommandHandler;
@property CommandHandlerBlock stopCommandHandler;
@property SelectAnimationHandlerBlock selectAnimationHandler;

- (void)registerPlayPressedHandler:(CommandHandlerBlock)handler;
- (void)registerPausePressedHandler:(CommandHandlerBlock)handler;
- (void)registerStopPressedHandler:(CommandHandlerBlock)handler;
- (void)registerSelectAnimationHandler:(SelectAnimationHandlerBlock)handler;

- (void)animationPlaybackEnded;

@end

NS_ASSUME_NONNULL_END
