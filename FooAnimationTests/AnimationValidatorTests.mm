//
//  AnimationValidatorTests.m
//  FooAnimationTests
//
//  Created by Jeffrey Bakker on 2021-12-27.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#import "AnimationValidator.hpp"

#import "AnimationTypes.hpp"
#import "AggregateCannedAnimationLoader.hpp"
#import "IncrementalSpritesAnimationLoader.hpp"
#import "RandomRelationalSpritesAnimationLoader.hpp"
#import "RandomSwarmSpritesAnimationLoader.hpp"
#import "Logger.hpp"

#import <XCTest/XCTest.h>

#import <memory>

using namespace animation::Implementation;
using namespace animation::Implementation::DataSources;
using namespace animation::Model;
using namespace logging::Implementation;
using namespace logging::Interfaces;
using namespace std;

@interface AnimationValidatorTests : XCTestCase {

	unique_ptr<AnimationValidator> m_uut;
}

@end

@implementation AnimationValidatorTests

- (void)setUp {
	shared_ptr<ILogger> logger = make_shared<Logger>();
	m_uut = make_unique<AnimationValidator>(logger);
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

- (void)testHasAtLeastTwoKeyframesFailed {

	Animation animation;
	auto frame0 = make_shared<Keyframe>();
	frame0->timestamp = DurationInSeconds{0.0f};
	animation.keyframes.push_back(frame0);

	bool result = m_uut->validate(animation);

	XCTAssertFalse(result);
}

- (void)testHasFirstKeyframeAtTimestampZeroFailed {

	Animation animation;
	auto frame0 = make_shared<Keyframe>();
	frame0->timestamp = DurationInSeconds{0.11f};
	animation.keyframes.push_back(frame0);

	auto frame1 = make_shared<Keyframe>();
	frame1->timestamp = DurationInSeconds{1.11f};
	animation.keyframes.push_back(frame1);

	bool result = m_uut->validate(animation);

	XCTAssertFalse(result);
}

- (void)testHasSequentialKeyframeTimestampsFailedEqual {

	Animation animation;

	auto frame0 = make_shared<Keyframe>();
	frame0->timestamp = DurationInSeconds{0.0f};
	animation.keyframes.push_back(frame0);

	auto frame1 = make_shared<Keyframe>();
	frame1->timestamp = DurationInSeconds{0.5f};
	animation.keyframes.push_back(frame1);

	auto frame2 = make_shared<Keyframe>();
	frame2->timestamp = DurationInSeconds{0.5f};
	animation.keyframes.push_back(frame2);

	bool result = m_uut->validate(animation);

	XCTAssertFalse(result);
}

- (void)testHasSequentialKeyframeTimestampsFailedLessThan {

	Animation animation;

	auto frame0 = make_shared<Keyframe>();
	frame0->timestamp = DurationInSeconds{0.0f};
	animation.keyframes.push_back(frame0);

	auto frame1 = make_shared<Keyframe>();
	frame1->timestamp = DurationInSeconds{0.500f};
	animation.keyframes.push_back(frame1);

	auto frame2 = make_shared<Keyframe>();
	frame2->timestamp = DurationInSeconds{0.499f};
	animation.keyframes.push_back(frame2);

	bool result = m_uut->validate(animation);

	XCTAssertFalse(result);
}

- (void)testAnimationLoaderAnimationsValid {

	auto loader = make_unique<AggregateCannedAnimationLoader>(
		make_unique<IncrementalSpritesAnimationLoader>(),
		make_unique<RandomRelationalSpritesAnimationLoader>(),
		make_unique<RandomSwarmSpritesAnimationLoader>()
	);

	auto animation1 = loader->getAnimations(0)[0];
	XCTAssertTrue(m_uut->validate(animation1));

	auto animation2 = loader->getAnimations(1)[0];
	XCTAssertTrue(m_uut->validate(animation2));

	auto animation3 = loader->getAnimations(2)[0];
	XCTAssertTrue(m_uut->validate(animation3));
}


@end
