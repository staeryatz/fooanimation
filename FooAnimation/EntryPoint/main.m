//
//  main.m
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-01.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
	@autoreleasepool {
	    // Setup code that might create autoreleased objects goes here.
	}
	return NSApplicationMain(argc, argv);
}
