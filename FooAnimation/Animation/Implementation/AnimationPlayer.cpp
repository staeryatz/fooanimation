//
//  AnimationPlayer.cpp
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-04.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#include "AnimationPlayer.hpp"

#include "AnimationTypes.hpp"

#include <fmt/core.h>

#include <chrono>
#include <iostream>

using namespace std;

namespace animation {
	namespace Implementation {

AnimationPlayer::AnimationPlayer(
	shared_ptr<ILogger> logger,
	shared_ptr<IPlaybackTimer> playbackTimer,
	shared_ptr<IKeyframeInterpolator> interpolator)
	: m_logger(logger)
	, m_timer(playbackTimer)
	, m_interpolator(move(interpolator))
{
}

AnimationPlayer::~AnimationPlayer()
{
	stop();
}

void AnimationPlayer::registerFrameCallback(FrameCallback callback)
{
	m_frameCallback = callback;
}

void AnimationPlayer::registerPlaybackEndedCallback(PlaybackEnded callback)
{
	m_playbackEndedCallback = callback;

	__block auto blockEndedCallback = m_playbackEndedCallback;
	m_timer->registerTimerEnded(^(){
		blockEndedCallback();
	});
}

void AnimationPlayer::selectAnimation(Animation& animation)
{
	m_animation = animation;
	m_logger->log(fmt::format("Selected animation '{}'", m_animation.name));
}

void AnimationPlayer::play()
{
	if (m_timer->isActive())
	{
		m_logger->log(fmt::format("Resuming animation playback"));
		m_timer->resume();
		return;
	}

	m_logger->log(fmt::format("Starting animation playback"));

	auto firstKeyframe = m_animation.keyframes[0];
	m_frameCallback(firstKeyframe);

	__block auto lastKeyframeIndex = 0;
	__block auto nextKeyframeIndex = 1;
	__block auto blockAnimation = m_animation;
	__block auto blockCallback = m_frameCallback;
	__block auto blockLogger = m_logger;

	m_timer->registerTimerIntervalFired(^(TimerPlaybackInfo playbackInfo) {

		auto elapsedTotal = playbackInfo.elapsed;

		auto lastKeyframe = blockAnimation.keyframes[lastKeyframeIndex];
		auto nextKeyframe = blockAnimation.keyframes[nextKeyframeIndex];

		auto nextKeyframeTime = nextKeyframe->timestamp;
		if (nextKeyframeTime.count() <= elapsedTotal.count())
		{
			lastKeyframeIndex = nextKeyframeIndex++;
		}

		if (nextKeyframeTime.count() > elapsedTotal.count() &&
			lastKeyframe->timestamp.count() < elapsedTotal.count())
		{
			blockLogger->log(fmt::format("Interpolating frame at: {:f} seconds", elapsedTotal.count()));
			auto tweenFrame = m_interpolator->interpolate(lastKeyframe, nextKeyframe, elapsedTotal);
			blockCallback(tweenFrame);
		}
		else if (nextKeyframeTime.count() <= elapsedTotal.count())
		{
			blockLogger->log(fmt::format("Displaying keyframe at: {:f} seconds", elapsedTotal.count()));
			blockCallback(nextKeyframe);
		}

		auto endKeyframe = blockAnimation.keyframes[blockAnimation.keyframes.size() - 1];
		if (endKeyframe->timestamp.count() <= elapsedTotal.count())
		{
			blockLogger->log(fmt::format("Animation has reached the end"));
			stop();
		}
	});

	m_timer->start();
}

void AnimationPlayer::pause()
{
	m_logger->log(fmt::format("Pausing animation playback"));
	m_timer->pause();
}

void AnimationPlayer::stop()
{
	m_logger->log(fmt::format("Stopping animation playback"));
	m_timer->stop();
}

} // namespace Implementation
} // namespace animation
