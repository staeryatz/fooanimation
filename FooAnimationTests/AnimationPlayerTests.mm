//
//  AnimationPlayerTests.m
//  FooAnimationTests
//
//  Created by Jeffrey Bakker on 2021-12-18.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#import "AnimationPlayer.hpp"
#import "AnimationTypes.hpp"
#import "KeyframeInterpolator.hpp"
#import "IPlaybackTimer.hpp"
#import "Logger.hpp"

#import <XCTest/XCTest.h>

#import <chrono>
#import <memory>

using namespace animation::Implementation;
using namespace animation::Interfaces;
using namespace logging::Implementation;
using namespace logging::Interfaces;
using namespace std;

class FakeTimer : public IPlaybackTimer
{
public:
	void registerTimerIntervalFired(TimerIntervalFiredCallback callback) override
	{
		m_intervalCallback = callback;
	}

	void registerTimerEnded(TimerEndedCallback callback) override
	{
		m_endedCallback = callback;
	}

	void start() override
	{
		m_startTime = chrono::steady_clock::now();
	}

	void stop() override
	{
		if (m_endedCallback)
		{
			m_endedCallback();
		}
	}

	void pause() override {}
	void resume() override {}

	bool isActive() override { return false; }

	void triggerInterval()
	{
		TimerPlaybackInfo info;
		info.elapsed = DurationInSeconds{0.0f};

		if (m_intervalCallback)
		{
			m_intervalCallback(info);
		}
	}

	void triggerInterval(TimerPlaybackInfo info)
	{
		m_intervalCallback(info);
	}

private:
	TimerIntervalFiredCallback m_intervalCallback;
	TimerEndedCallback m_endedCallback;

	chrono::time_point<chrono::steady_clock> m_startTime;
};


@interface AnimationPlayerTests : XCTestCase {
	unique_ptr<AnimationPlayer> m_uut;
	shared_ptr<FakeTimer> m_timer;
}
@end

@implementation AnimationPlayerTests

- (void)setUp {

	shared_ptr<ILogger> logger = make_shared<Logger>();
	shared_ptr<IKeyframeInterpolator> interpolator = make_shared<KeyframeInterpolator>();
	m_timer = make_shared<FakeTimer>();

	m_uut = make_unique<AnimationPlayer>(logger, m_timer, interpolator);
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

- (Animation)createTestAnimation {

    Animation animation;

	auto frame0 = make_shared<Keyframe>();
	frame0->timestamp = DurationInSeconds{0.0f};

	auto frame1 = make_shared<Keyframe>();
	frame1->timestamp = DurationInSeconds{0.5f};

	animation.keyframes.push_back(frame0);
	animation.keyframes.push_back(frame1);

	return animation;
}

- (void)testFirstKeyFrameCallbackFiredImmediately {

	auto animation = [self createTestAnimation];

	bool isFirstKeyframe = false;
	bool frameCallbackFired = false;
	m_uut->registerFrameCallback([&](AnimationFramePtr frame) {

		frameCallbackFired = true;

		Keyframe& castFrame = reinterpret_cast<Keyframe&>(*frame.get());
		isFirstKeyframe = (castFrame.timestamp == animation.keyframes[0]->timestamp);
	});

    m_uut->selectAnimation(animation);
    m_uut->play();

	XCTAssertTrue(frameCallbackFired);
	XCTAssertTrue(isFirstKeyframe);
}

- (void)testTweenFrameCallbackFired {

	auto animation = [self createTestAnimation];

	bool isKeyframe = true;
	auto frameCallbackFiredCount = 0;
	m_uut->registerFrameCallback(
		[&isKeyframe, &frameCallbackFiredCount](AnimationFramePtr frame) {

		frameCallbackFiredCount++;

//		isKeyframe = frame->isKey();
		Keyframe& castFrame = reinterpret_cast<Keyframe&>(*frame.get());
		Keyframe keyframe;

		NSLog(@"castFrame typeid: %s, keyframe typeid: %s",
			typeid(castFrame).name(), typeid(keyframe).name());

		auto result = strcmp(typeid(keyframe).name(), typeid(castFrame).name());
		isKeyframe = (result == 0);
	});

    m_uut->selectAnimation(animation);
    m_uut->play();

	TimerPlaybackInfo timeInfo;
	timeInfo.elapsed = DurationInSeconds{0.05f};
    m_timer->triggerInterval(timeInfo);

	// Once immediately on play() and another from the interval trigger
	XCTAssertTrue(frameCallbackFiredCount == 2);
	XCTAssertFalse(isKeyframe);
}

- (void)testKeyFrameCallbackFired {

    auto animation = [self createTestAnimation];

	bool isKeyframe = false;
	auto frameCallbackFiredCount = 0;
	m_uut->registerFrameCallback(
		[&isKeyframe, &frameCallbackFiredCount](AnimationFramePtr frame) {

		frameCallbackFiredCount++;

//		isKeyframe = frame->isKey();
		Keyframe& castFrame = reinterpret_cast<Keyframe&>(*frame.get());
		Keyframe keyframe;

		NSLog(@"castFrame typeid: %s, keyframe typeid: %s",
			typeid(castFrame).name(), typeid(keyframe).name());

		auto result = strcmp(typeid(keyframe).name(), typeid(castFrame).name());
		isKeyframe = (result == 0);
	});

    m_uut->selectAnimation(animation);
    m_uut->play();

	TimerPlaybackInfo timeInfo;
	timeInfo.elapsed = DurationInSeconds{0.5f};
    m_timer->triggerInterval(timeInfo);

	// Once immediately on play() and another from the interval trigger
	XCTAssertTrue(frameCallbackFiredCount == 2);
	XCTAssertTrue(isKeyframe);
}

- (void)testTweenAndKeyFrameCallbackFired {

	auto animation = [self createTestAnimation];

	auto isKeyframeCount = 0;
	auto frameCallbackFiredCount = 0;
	m_uut->registerFrameCallback([&](AnimationFramePtr frame) {
		frameCallbackFiredCount++;

		Keyframe& castFrame = reinterpret_cast<Keyframe&>(*frame.get());
		Keyframe keyframe;

		auto result = strcmp(typeid(keyframe).name(), typeid(castFrame).name());
		if (result == 0)
		{
			isKeyframeCount++;
		}
	});

    m_uut->selectAnimation(animation);
    m_uut->play();

	const int expectedFrames = 10;
	const int expectedKeyframes = 2;

	TimerPlaybackInfo timeInfo;

	for (int i = 1; i <= expectedFrames; i++)
	{
		timeInfo.elapsed = DurationInSeconds{0.05 * i};
		m_timer->triggerInterval(timeInfo);
	}

	// Once immediately on play() and then the expected from the interval trigger
	XCTAssertTrue(frameCallbackFiredCount == expectedFrames + 1);
	XCTAssertTrue(isKeyframeCount == expectedKeyframes);
}

- (void)testPlaybackEndedOnExactlyLastKeyframe {

    auto animation = [self createTestAnimation];

	bool frameCallbackFired = false;
	m_uut->registerFrameCallback([&frameCallbackFired](AnimationFramePtr frame) {
		frameCallbackFired = true;
	});

	bool animationPlaybackEnded = false;
	m_uut->registerPlaybackEndedCallback([&animationPlaybackEnded](){
		animationPlaybackEnded = true;
	});

    m_uut->selectAnimation(animation);
    m_uut->play();

	TimerPlaybackInfo timeInfo;
	timeInfo.elapsed = DurationInSeconds{0.5f};
    m_timer->triggerInterval(timeInfo);

	XCTAssertTrue(frameCallbackFired);
	XCTAssertTrue(animationPlaybackEnded);
}

- (void)testPlaybackEndedAfterLastKeyframe {

    auto animation = [self createTestAnimation];

	bool frameCallbackFired = false;
	m_uut->registerFrameCallback([&frameCallbackFired](AnimationFramePtr frame) {
		frameCallbackFired = true;
	});

	bool animationPlaybackEnded = false;
	m_uut->registerPlaybackEndedCallback([&animationPlaybackEnded]() {
		animationPlaybackEnded = true;
	});

    m_uut->selectAnimation(animation);
    m_uut->play();

	TimerPlaybackInfo timeInfo;
	timeInfo.elapsed = DurationInSeconds{0.550f};
    m_timer->triggerInterval(timeInfo);

	XCTAssertTrue(frameCallbackFired);
	XCTAssertTrue(animationPlaybackEnded);
}

@end
