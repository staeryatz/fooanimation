//
//  AppDelegate.h
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-01.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

- (void)setupAnimationView;

@end
