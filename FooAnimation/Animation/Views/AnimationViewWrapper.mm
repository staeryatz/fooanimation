//
//  AnimationViewWrapper.mm
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-24.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#include "AnimationViewWrapper.hpp"

namespace animation {
	namespace Views {

AnimationViewWrapper::AnimationViewWrapper(id<AnimationViewProtocol> underlyingView)
{
	m_underlyingView = underlyingView;
}

AnimationViewWrapper::~AnimationViewWrapper()
{
	m_underlyingView = nil;
}

void AnimationViewWrapper::loadScene(ScenePtr scene)
{
	[m_underlyingView loadScene:scene];
}

void AnimationViewWrapper::updateFrame(AnimationFramePtr frame)
{
	[m_underlyingView updateFrame:frame];
}

void AnimationViewWrapper::displayText(std::string text)
{
	[m_underlyingView displayText:[NSString stringWithUTF8String:text.c_str()]];
}

} // namespace Views
} // namespace animation

