//
//  UiAnimationView.h
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-03.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#ifndef UiAnimationView_h
#define UiAnimationView_h

#import "AnimationViewProtocol.h"

#import "AnimationPlayerViewKeystrokeBase.h"
#import "AnimationTypes.hpp"

#import <Cocoa/Cocoa.h>

using namespace animation::Model;

@interface UiAnimationView : AnimationPlayerViewKeystrokeBase<AnimationViewProtocol>

@property ScenePtr scene;
@property AnimationFramePtr currentFrame;
@property NSString* text;

- (void)loadScene:(ScenePtr)scene;
- (void)updateFrame:(AnimationFramePtr)frame;
- (void)displayText:(NSString*)text;
- (void)drawText;

- (void)drawSprite:(SpriteState)spriteState atScaledOffset:(CGPoint)point;
- (NSColor*)convertColor:(animation::Model::Color&)color;

- (CGPoint)scaleToCoordinateSystem:(CGFloat)x andY:(CGFloat)y;
@end

#endif /* UiAnimationView_h */
