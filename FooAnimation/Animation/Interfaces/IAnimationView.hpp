//
//  IAnimationView.h
//  FooAnimation
//
//  Created by Jeffrey Bakker on 2021-12-03.
//  Copyright © 2021 Jeffrey Bakker. All rights reserved.
//

#ifndef IAnimationView_h
#define IAnimationView_h

#include "AnimationTypes.hpp"

#include <string>

using namespace animation::Model;

namespace animation {
	namespace Interfaces {

class IAnimationView
{
public:
	virtual ~IAnimationView() = default;
	virtual void loadScene(ScenePtr scene) = 0;
	virtual void updateFrame(AnimationFramePtr frame) = 0;
	virtual void displayText(std::string text) = 0;
};

} // namespace Interfaces
} // namespace animation

#endif /* IAnimationView_h */
